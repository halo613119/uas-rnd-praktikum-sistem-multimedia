## No 6 

###### Program yang saya  berikan adalah sebuah program yang menggunakan teknik pemrosesan multimedia dan kecerdasan buatan (AI) untuk mengenali dan mengklasifikasikan gambar tumor otak sebagai "Tumor" atau "Normal". Program ini menggunakan teknologi pengolahan citra untuk memproses gambar-gambar otak dan mendeteksi adanya tumor. Dengan menggunakan model jaringan saraf tiruan (neural network) berbasis konvolusi (CNN), program ini dapat mengenali pola-pola dalam gambar yang mengindikasikan adanya tumor otak.

###### List Use Case:
###### 1. Deteksi Tumor Otak: Produk ini dapat digunakan oleh dokter dan profesional medis dalam proses deteksi tumor otak berdasarkan citra MRI otak. Dengan menggunakan teknologi pengenalan citra AI, sistem dapat secara otomatis mengidentifikasi adanya tumor otak pada gambar MRI dengan akurasi tinggi, membantu dalam diagnosis awal dan pengobatan yang tepat.
###### 2. Pengenalan Pola Citra Medis: Produk ini dapat digunakan untuk pengenalan pola citra medis lainnya selain deteksi tumor otak. Contohnya, sistem dapat mengidentifikasi pola citra yang berkaitan dengan jenis penyakit tertentu seperti kanker payudara atau penyakit jantung, membantu dalam diagnosis dan penanganan yang lebih efisien.
###### 3. Generasi Otomatis Citra Medis: Produk ini dapat digunakan untuk menghasilkan citra medis yang berkualitas tinggi secara otomatis. Contohnya, sistem dapat menghasilkan citra MRI otak yang diperbaiki atau ditingkatkan untuk tujuan penelitian atau pendidikan medis. Citra-citra yang dihasilkan oleh sistem dapat membantu dokter dan peneliti dalam menganalisis dan memahami lebih baik kondisi medis yang kompleks.
###### 4. Pengolahan dan Pemampatan Multimedia Medis: Produk ini dapat digunakan untuk pengolahan dan pemampatan data multimedia medis seperti citra MRI otak. Sistem dapat mengimplementasikan algoritma pemrosesan citra dan teknik kompresi multimedia untuk mengoptimalkan penyimpanan dan pengiriman data medis yang lebih efisien.

## No 7
![demo-image](https://gitlab.com/halo613119/uas-rnd-praktikum-sistem-multimedia/-/raw/main/Yes.png)
###### Pada gambar di atas adalah hasil percobaan untuk mengecek apakah gambar tersebut termasuk tumor otak atau bukan, dan didapatkan akurasi percobaan sebesar 99.9% tumor otak.
![demo-image](https://gitlab.com/halo613119/uas-rnd-praktikum-sistem-multimedia/-/raw/main/No.png)
###### Pada gambar di atas adalah hasil percobaan untuk mengecek apakah gambar tersebut termasuk tumor otak atau bukan, dan didapatkan akurasi percobaan sebesar 100% bukan tumor otak.
## No 8
###### Berikut link YouTube Presentasi : https://youtu.be/ngnkRo0IPGw
## No 9

## No 10 
###### Berikut adalah link Google Docs untuk artikel https://docs.google.com/document/d/13ujCmHBcK91f08iGtKNO4vSm6Jukg9HU/edit?usp=sharing&ouid=108502459826880490656&rtpof=true&sd=true
